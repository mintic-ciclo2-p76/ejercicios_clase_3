import java.util.Scanner;
public class App {
    public static void main(String[] args) throws Exception {
        //ejercicio_4();
        ejercicio_5();
    }
    // Ejercicio 4 (clase 3)
    public static void ejercicio_4() {
        try(Scanner Leer = new Scanner(System.in)){
            System.out.println("------Km/h a m/s------");
            System.out.print("Ingrese una velocidad en Km/h: ");
            double kilometros_horas = Leer.nextDouble();
            System.out.println("La velocidad en m/s es: "+ kilometros_horas*1000/3600);
        }
    }
    // Ejercicio 5 (clase 3)
    public static void ejercicio_5() {
        try(Scanner Leer = new Scanner(System.in)){
            System.out.println("---TEOREMA DE PITAGORAS---");
            System.out.print("Ingrese la longitud de un cateto del triangulo rectangulo: ");
            double a = Leer.nextDouble();
            System.out.print("Ingrese la longitud del otro cateto  del triangulo rectangulo: ");
            double b = Leer.nextDouble();
            double hipotenusa = Math.sqrt(Math.pow(a, 2)+ Math.pow(b, 2));
            System.out.println("La hipotenusa del triangulo rectangulo es: "+ hipotenusa);
        }
        
    }
}
